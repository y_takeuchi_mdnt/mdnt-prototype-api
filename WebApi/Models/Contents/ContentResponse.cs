namespace WebApi.Models.Contents;

public class ContentResponse
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Path { get; set; }
    public DateTime Created { get; set; }
    public DateTime? Updated { get; set; }
}