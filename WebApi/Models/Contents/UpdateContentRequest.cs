namespace WebApi.Models.Contents;

public class UpdateContentRequest
{
    public string? Name { get; set; }

    public string? Path { get; set; }
}