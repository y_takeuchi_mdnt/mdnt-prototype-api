namespace WebApi.Models.Contents;

using System.ComponentModel.DataAnnotations;

public class CreateContentRequest
{
    [Required]
    public string? Name { get; set; }

    [Required]
    public string? Path { get; set; }
}