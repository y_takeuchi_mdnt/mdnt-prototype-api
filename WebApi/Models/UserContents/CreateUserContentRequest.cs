namespace WebApi.Models.UserContents;

using System.ComponentModel.DataAnnotations;

public class CreateUserContentRequest
{
    [Required]
    public int AccountId { get; set; }
    [Required]
    public int ContentId { get; set; }
}