namespace WebApi.Models.UserContents;

using System.ComponentModel.DataAnnotations;

public class DeleteUserContentRequest
{
    [Required]
    public int AccountId { get; set; }
    [Required]
    public int ContentId { get; set; }
}