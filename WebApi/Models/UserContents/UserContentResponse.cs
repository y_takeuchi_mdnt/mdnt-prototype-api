namespace WebApi.Models.UserContents;

public class UserContentResponse
{
    public int Id { get; set; }
    public string? AccountId { get; set; }
    public string? ContentId { get; set; }
    public DateTime Created { get; set; }
    public DateTime? Updated { get; set; }
}