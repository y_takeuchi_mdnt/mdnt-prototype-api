namespace WebApi.Helpers;

using AutoMapper;
using WebApi.Entities;
using WebApi.Models.Accounts;
using WebApi.Models.Contents;
using WebApi.Models.UserContents;

/// <summary>
/// エンティティとモデルのマッピング
/// </summary>
public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<Account, AccountResponse>();
        CreateMap<Account, AuthenticateResponse>();
        CreateMap<RegisterRequest, Account>();
        CreateMap<CreateRequest, Account>();
        CreateMap<UpdateRequest, Account>()
            .ForAllMembers(x => x.Condition(
                (src, dest, prop) =>
                {
                    // nullと空の文字列を無視する
                    if (prop == null) return false;
                    if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                    // 権限がnullのときは無視する
                    if (x.DestinationMember.Name == "Role" && src.Role == null) return false;

                    return true;
                }
            ));

        CreateMap<Content, ContentResponse>();
        CreateMap<CreateContentRequest, Content>();
        CreateMap<UpdateContentRequest, Content>()
            .ForAllMembers(x => x.Condition(
                (src, dest, prop) =>
                {
                    // nullと空の文字列を無視する
                    if (prop == null) return false;
                    if (prop.GetType() == typeof(string) && string.IsNullOrEmpty((string)prop)) return false;

                    return true;
                }
            ));

        CreateMap<UserContent, UserContentResponse>();
        CreateMap<CreateUserContentRequest, UserContent>();
    }
}