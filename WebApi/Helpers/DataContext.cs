namespace WebApi.Helpers;

using Microsoft.EntityFrameworkCore;
using WebApi.Entities;

public class DataContext : DbContext
{
    public DbSet<Account> Accounts { get; set; }
    public DbSet<Content> Contents { get; set; }
    public DbSet<UserContent> UserContents { get; set; }

    private readonly IConfiguration Configuration;

#pragma warning disable CS8618
    public DataContext(IConfiguration configuration)
#pragma warning restore CS8618
    {
        Configuration = configuration;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        options.UseSqlServer(Configuration.GetConnectionString("WebApiDatabase"));
    }
}