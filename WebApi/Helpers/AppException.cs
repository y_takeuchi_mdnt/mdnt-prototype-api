namespace WebApi.Helpers;

using System.Globalization;

/// <summary>
/// アプリケーション固有の例外をスローするためのカスタム例外クラス
/// </summary>
public class AppException : Exception
{
    public AppException() : base() { }

    public AppException(string message) : base(message) { }

    public AppException(string message, params object[] args) : base(string.Format(CultureInfo.CurrentCulture, message, args)) { }
}