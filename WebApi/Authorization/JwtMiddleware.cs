namespace WebApi.Authorization;
using WebApi.Helpers;

public class JwtMiddleware
{
    private readonly RequestDelegate _next;

    public JwtMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context, DataContext dataContext, IJwtUtils jwtUtils)
    {
        var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
        var accountId = jwtUtils.ValidateJwtToken(token);
        if (accountId != null)
        {
            // JWT認証が成功したらコンテキストにアカウントを設定する
            context.Items["Account"] = await dataContext.Accounts.FindAsync(accountId);
        }

        await _next(context);
    }
}