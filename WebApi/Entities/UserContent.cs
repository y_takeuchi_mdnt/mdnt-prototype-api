namespace WebApi.Entities;

public class UserContent
{
    public int Id { get; set; }
    public int AccountId { get; set; }
    public int ContentId { get; set; }
    public DateTime Created { get; set; }
    public DateTime? Updated { get; set; }
}