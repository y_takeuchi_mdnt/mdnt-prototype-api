namespace WebApi.Entities;

public class Content
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Path { get; set; }
    public DateTime Created { get; set; }
    public DateTime? Updated { get; set; }
}