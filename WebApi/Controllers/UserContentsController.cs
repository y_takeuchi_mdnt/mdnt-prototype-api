﻿namespace WebApi.Controllers;

using Microsoft.AspNetCore.Mvc;
using WebApi.Authorization;
using WebApi.Models.UserContents;
using WebApi.Services;

[Authorize]
[ApiController]
[Route("[controller]")]
public class UserContentsController : BaseController
{
    private readonly IUserContentService _userContentService;

    public UserContentsController(IUserContentService userContentService)
    {
        _userContentService = userContentService;
    }

    [HttpGet("{accountId:int}")]
    public ActionResult<IEnumerable<UserContentResponse>> GetAll(int accountId)
    {
        var contents = _userContentService.GetAll(accountId);
        return Ok(contents);
    }

    [HttpPost]
    public ActionResult<UserContentResponse> Create(CreateUserContentRequest model)
    {
        var account = _userContentService.Create(model);
        return Ok(account);
    }

    [HttpDelete]
    public IActionResult Delete(DeleteUserContentRequest model)
    {
        _userContentService.Delete(model);
        return Ok(new { message = "Content deleted successfully" });
    }
}