﻿namespace WebApi.Controllers;

using Microsoft.AspNetCore.Mvc;
using WebApi.Entities;

[Controller]
public abstract class BaseController : ControllerBase
{
    /// <summary>
    /// 認証されているアカウントを返却（ログインしていないときはnull）
    /// </summary>
    public Account? Account => (Account?)HttpContext.Items["Account"];
}