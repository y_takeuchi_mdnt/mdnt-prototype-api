﻿namespace WebApi.Controllers;

using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Models.Contents;
using WebApi.Services;

[Authorize(Role.Admin)]
[ApiController]
[Route("[controller]")]
public class ContentsController : BaseController
{
    private readonly IContentService _contentService;
    private readonly string _contentSaveDirectory = "C:\\inetpub\\wwwroot\\wwwroot\\contents";
    private readonly long _contentMaxFileSize = 1024 * 1024 * 100;

    public ContentsController(IContentService contentService)
    {
        _contentService = contentService;
    }

    [HttpGet]
    public ActionResult<IEnumerable<ContentResponse>> GetAll()
    {
        var contents = _contentService.GetAll();
        return Ok(contents);
    }

    [HttpGet("{id:int}")]
    public ActionResult<ContentResponse> GetById(int id)
    {
        var content = _contentService.GetById(id);
        return Ok(content);
    }

    [HttpPost]
    public async Task<ActionResult<ContentResponse>> Create([FromForm] IFormFile file, [FromForm] string json)
    {
        var model = JsonSerializer.Deserialize<CreateContentRequest>(json);

        if (model == null || file.Length == 0)
            return BadRequest();
        else if (_contentMaxFileSize < file.Length)
            return BadRequest();
        else
            model.Path = await SaveFile(file);

        var content = _contentService.Create(model);
        return Ok(content);
    }

    [HttpPut("{id:int}")]
    public async Task<ActionResult<ContentResponse>> Update(int id, [FromForm] IFormFile file, [FromForm] string json)
    {
        var model = JsonSerializer.Deserialize<UpdateContentRequest>(json);

        if (model == null || file.Length == 0)
            return BadRequest();
        else if (_contentMaxFileSize < file.Length)
            return BadRequest();
        else
        {
            var f = new FileInfo(Path.Combine(_contentSaveDirectory, model.Path!));
            f.Delete();
            model.Path = await SaveFile(file);
        }

        var content = _contentService.Update(id, model);
        return Ok(content);
    }

    [HttpDelete("{id:int}")]
    public IActionResult Delete(int id)
    {
        _contentService.Delete(id);
        return Ok(new { message = "Content deleted successfully" });
    }

    private async Task<string> SaveFile(IFormFile file)
    {
        var fileName = Path.ChangeExtension(Path.GetRandomFileName(), ".mp4");
        var path = Path.Combine(_contentSaveDirectory, fileName);

        await using FileStream fs = new(path, FileMode.Create);
        await file.CopyToAsync(fs);

        return fileName;
    }
}