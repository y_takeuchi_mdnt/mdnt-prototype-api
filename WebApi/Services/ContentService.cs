﻿namespace WebApi.Services;

using AutoMapper;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Contents;

public interface IContentService
{
    IEnumerable<ContentResponse> GetAll();
    ContentResponse GetById(int id);
    ContentResponse Create(CreateContentRequest model);
    ContentResponse Update(int id, UpdateContentRequest model);
    void Delete(int id);
}

public class ContentService : IContentService
{
    private readonly DataContext _context;
    private readonly IMapper _mapper;

    public ContentService(DataContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public IEnumerable<ContentResponse> GetAll()
    {
        var contents = _context.Contents;
        return _mapper.Map<IList<ContentResponse>>(contents);
    }

    public ContentResponse GetById(int id)
    {
        var content = GetContent(id);
        return _mapper.Map<ContentResponse>(content);
    }

    public ContentResponse Create(CreateContentRequest model)
    {
        var content = _mapper.Map<Content>(model);
        content.Created = DateTime.UtcNow;

        _context.Contents.Add(content);
        _context.SaveChanges();

        return _mapper.Map<ContentResponse>(content);
    }

    public ContentResponse Update(int id, UpdateContentRequest model)
    {
        var content = GetContent(id);

        _mapper.Map(model, content);
        content.Updated = DateTime.UtcNow;

        _context.Contents.Update(content);
        _context.SaveChanges();

        return _mapper.Map<ContentResponse>(content);
    }

    public void Delete(int id)
    {
        var content = GetContent(id);
        _context.Contents.Remove(content);
        _context.SaveChanges();
    }

    private Content GetContent(int id)
    {
        var content = _context.Contents.Find(id);
        if (content == null) throw new KeyNotFoundException("Content not found");
        return content;
    }
}