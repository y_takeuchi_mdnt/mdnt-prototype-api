namespace WebApi.Services;

using AutoMapper;
using System.Security.Cryptography;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Accounts;
using BC = BCrypt.Net.BCrypt;

public interface IAccountService
{
    AuthenticateResponse Authenticate(AuthenticateRequest model);
    void Register(RegisterRequest model, string origin);
    void VerifyEmail(string? token);
    void ForgotPassword(ForgotPasswordRequest model, string origin);
    void ValidateResetToken(ValidateResetTokenRequest model);
    void ResetPassword(ResetPasswordRequest model);
    IEnumerable<AccountResponse> GetAll();
    AccountResponse GetById(int id);
    AccountResponse Create(CreateRequest model);
    AccountResponse Update(int id, UpdateRequest model);
    void Delete(int id);
}

public class AccountService : IAccountService
{
    private readonly DataContext _context;
    private readonly IMapper _mapper;
    private readonly IEmailService _emailService;
    private readonly IJwtUtils _jwtUtils;

    public AccountService(
        DataContext context,
        IMapper mapper,
        IEmailService emailService,
        IJwtUtils jwtUtils)
    {
        _context = context;
        _mapper = mapper;
        _emailService = emailService;
        _jwtUtils = jwtUtils;
    }

    /// <summary>
    /// 認証
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public AuthenticateResponse Authenticate(AuthenticateRequest model)
    {
        var account = _context.Accounts.SingleOrDefault(x => x.Email == model.Email);

        if (account == null || !account.IsVerified || !BC.Verify(model.Password, account.PasswordHash))
            throw new AppException("メールアドレスまたはパスワードが間違っています。");

        //_context.Update(account);
        //_context.SaveChanges();

        var response = _mapper.Map<AuthenticateResponse>(account);
        response.JwtToken = _jwtUtils.GenerateJwtToken(account);
        return response;
    }

    /// <summary>
    /// アカウント登録
    /// </summary>
    /// <param name="model"></param>
    /// <param name="origin"></param>
    public void Register(RegisterRequest model, string origin)
    {
        if (_context.Accounts.Any(x => x.Email == model.Email))
        {
            SendAlreadyRegisteredEmail(model.Email, origin);
            return;
        }

        var account = _mapper.Map<Account>(model);

        // 最初に登録された場合は管理者とする
        var isFirstAccount = !_context.Accounts.Any();
        account.Role = isFirstAccount ? Role.Admin : Role.User;
        account.Created = DateTime.UtcNow;
        account.VerificationToken = RandomTokenString();
        account.PasswordHash = BC.HashPassword(model.Password);

        _context.Accounts.Add(account);
        _context.SaveChanges();

        SendVerificationEmail(account, origin);
    }

    /// <summary>
    /// アカウント確認
    /// </summary>
    /// <param name="token"></param>
    public void VerifyEmail(string? token)
    {
        var account = _context.Accounts.SingleOrDefault(x => x.VerificationToken == token);

        if (account == null) throw new AppException("Verification failed");

        account.Verified = DateTime.UtcNow;
        account.VerificationToken = null;

        _context.Accounts.Update(account);
        _context.SaveChanges();
    }

    /// <summary>
    /// パスワード忘れ
    /// </summary>
    /// <param name="model"></param>
    /// <param name="origin"></param>
    public void ForgotPassword(ForgotPasswordRequest model, string origin)
    {
        var account = _context.Accounts.SingleOrDefault(x => x.Email == model.Email);

        // 電子メールの列挙を防ぐために、常に200を返す
        if (account == null) return;

        // 1日後に期限切れになるリセットトークンを作成
        account.ResetToken = RandomTokenString();
        account.ResetTokenExpires = DateTime.UtcNow.AddDays(1);

        _context.Accounts.Update(account);
        _context.SaveChanges();

        SendPasswordResetEmail(account, origin);
    }

    /// <summary>
    /// リセットトークン検証
    /// </summary>
    /// <param name="model"></param>
    public void ValidateResetToken(ValidateResetTokenRequest model)
    {
        var account = _context.Accounts.SingleOrDefault(x =>
            x.ResetToken == model.Token &&
            x.ResetTokenExpires > DateTime.UtcNow);

        if (account == null)
            throw new AppException("Invalid token");
    }

    /// <summary>
    /// パスワードリセット
    /// </summary>
    /// <param name="model"></param>
    public void ResetPassword(ResetPasswordRequest model)
    {
        var account = _context.Accounts.SingleOrDefault(x =>
            x.ResetToken == model.Token &&
            x.ResetTokenExpires > DateTime.UtcNow);

        if (account == null)
            throw new AppException("Invalid token");

        // パスワードを更新し、リセットトークンを削除
        account.PasswordHash = BC.HashPassword(model.Password);
        account.PasswordReset = DateTime.UtcNow;
        account.ResetToken = null;
        account.ResetTokenExpires = null;

        _context.Accounts.Update(account);
        _context.SaveChanges();
    }

    /// <summary>
    /// すべてのアカウントを取得
    /// </summary>
    /// <returns></returns>
    public IEnumerable<AccountResponse> GetAll()
    {
        var accounts = _context.Accounts;
        return _mapper.Map<IList<AccountResponse>>(accounts);
    }

    /// <summary>
    /// アカウントを取得
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public AccountResponse GetById(int id)
    {
        var account = GetAccount(id);
        return _mapper.Map<AccountResponse>(account);
    }

    /// <summary>
    /// アカウント作成
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public AccountResponse Create(CreateRequest model)
    {
        if (_context.Accounts.Any(x => x.Email == model.Email))
            throw new AppException($"Email '{model.Email}' is already registered");

        var account = _mapper.Map<Account>(model);
        account.Created = DateTime.UtcNow;
        account.Verified = DateTime.UtcNow;
        account.PasswordHash = BC.HashPassword(model.Password);

        _context.Accounts.Add(account);
        _context.SaveChanges();

        return _mapper.Map<AccountResponse>(account);
    }

    /// <summary>
    /// アカウント更新
    /// </summary>
    /// <param name="id"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    public AccountResponse Update(int id, UpdateRequest model)
    {
        var account = GetAccount(id);

        if (account.Email != model.Email && _context.Accounts.Any(x => x.Email == model.Email))
            throw new AppException($"Email '{model.Email}' is already taken");

        if (!string.IsNullOrEmpty(model.Password))
            account.PasswordHash = BC.HashPassword(model.Password);

        _mapper.Map(model, account);
        account.Updated = DateTime.UtcNow;

        _context.Accounts.Update(account);
        _context.SaveChanges();

        return _mapper.Map<AccountResponse>(account);
    }

    /// <summary>
    /// アカウント削除
    /// </summary>
    /// <param name="id"></param>
    public void Delete(int id)
    {
        var account = GetAccount(id);
        _context.Accounts.Remove(account);
        _context.SaveChanges();
    }

    /// <summary>
    /// アカウントの取得
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private Account GetAccount(int id)
    {
        var account = _context.Accounts.Find(id);
        if (account == null) throw new KeyNotFoundException("Account not found");
        return account;
    }

    /// <summary>
    /// ランダムなトークンを取得
    /// </summary>
    /// <returns></returns>
    private string RandomTokenString()
    {
        return BitConverter.ToString(RandomNumberGenerator.GetBytes(40)).Replace("-", "");
    }

    /// <summary>
    /// 確認メール送信
    /// </summary>
    /// <param name="account"></param>
    /// <param name="origin"></param>
    private void SendVerificationEmail(Account account, string origin)
    {
        string message;
        if (!string.IsNullOrEmpty(origin))
        {
            var verifyUrl = $"{origin}/account/verify-email?token={account.VerificationToken}";
            message = $@"<p>以下のリンクをクリックして、メールアドレスを確認してください:</p>
                             <p><a href=""{verifyUrl}"">{verifyUrl}</a></p>";
        }
        else
        {
            message = $@"<p>以下のトークンを使用して、メールアドレスを確認してください <code>/accounts/verify-email</code> api route:</p>
                             <p><code>{account.VerificationToken}</code></p>";
        }

        _emailService.Send(
            to: account.Email ?? string.Empty,
            subject: "Sign-up Verification API - メール確認",
            html: $@"<h4>メール確認</h4>
                         <p>登録していただきありがとうございます！</p>
                         {message}"
        );
    }

    /// <summary>
    /// すでに登録されているメールに送信
    /// </summary>
    /// <param name="email"></param>
    /// <param name="origin"></param>
    private void SendAlreadyRegisteredEmail(string? email, string origin)
    {
        string message;
        if (!string.IsNullOrEmpty(origin))
            message = $@"<p>パスワードを忘れてしまった場合は、以下のリンクにアクセスしてください。<a href=""{origin}/account/forgot-password"">forgot password</a> page.</p>";
        else
            message = "<p>パスワードが分からない場合は、APIからリセットできます。<code>/accounts/forgot-password</code> api route.</p>";

        _emailService.Send(
            to: email ?? string.Empty,
            subject: "Sign-up Verification API - メール重複登録",
            html: $@"<h4>すでに登録されているメールアドレスです。</h4>
                         <p>あなたのメールアドレスは<strong>{email}</strong>すでに登録されています。</p>
                         {message}"
        );
    }

    /// <summary>
    /// パスワードリセットメールを送信
    /// </summary>
    /// <param name="account"></param>
    /// <param name="origin"></param>
    private void SendPasswordResetEmail(Account account, string origin)
    {
        string message;
        if (!string.IsNullOrEmpty(origin))
        {
            var resetUrl = $"{origin}/account/reset-password?token={account.ResetToken}";
            message = $@"<p>以下のリンクをクリックしてパスワードをリセットしてください。リンクは1日間有効です。</p>
                             <p><a href=""{resetUrl}"">{resetUrl}</a></p>";
        }
        else
        {
            message = $@"<p>以下のトークンを使用して、APIでパスワードをリセットしてください。<code>/accounts/reset-password</code> api route:</p>
                             <p><code>{account.ResetToken}</code></p>";
        }

        _emailService.Send(
            to: account.Email ?? string.Empty,
            subject: "Sign-up Verification API - パスワードリセット",
            html: $@"<h4>Reset Password Email</h4>
                         {message}"
        );
    }
}