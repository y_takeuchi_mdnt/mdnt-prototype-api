using AutoMapper;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.UserContents;

namespace WebApi.Services;

public interface IUserContentService
{
    IEnumerable<UserAllContentResponse> GetAll(int accountId);
    UserContentResponse Create(CreateUserContentRequest model);
    void Delete(DeleteUserContentRequest model);
}

public class UserContentService : IUserContentService
{
    private readonly DataContext _context;
    private readonly IMapper _mapper;
    private readonly AppSettings _appSettings;

    public UserContentService(
        DataContext context,
        IMapper mapper,
        IOptions<AppSettings> appSettings)
    {
        _context = context;
        _mapper = mapper;
        _appSettings = appSettings.Value;
    }
    public IEnumerable<UserAllContentResponse> GetAll(int accountId)
    {
        var a = _context.Contents.ToList();
        var b = _context.UserContents.Where(x => x.AccountId == accountId).ToList();

        var contents = a
            .GroupJoin(
                b,
                c => c.Id,
                u => u.ContentId,
                (content, userContent) => new
                {
                    content.Id,
                    content.Name,
                    content.Path,
                    UserContent = userContent.DefaultIfEmpty()
                }
            )
            .SelectMany
            (
                x => x.UserContent, (c, u) => new UserAllContentResponse
                {
                    Id = c.Id,
                    Name = c.Name,
                    Path = c.Path,
                    Check = u != null
                }
            );

        return contents;
    }

    public UserContentResponse Create(CreateUserContentRequest model)
    {
        var userContent = _mapper.Map<UserContent>(model);
        userContent.Created = DateTime.UtcNow;

        _context.UserContents.Add(userContent);
        _context.SaveChanges();

        return _mapper.Map<UserContentResponse>(userContent);
    }

    public void Delete(DeleteUserContentRequest model)
    {
        var userContent = GetUserContent(model.AccountId, model.ContentId);
        _context.UserContents.Remove(userContent);
        _context.SaveChanges();
    }

    private UserContent GetUserContent(int accountId, int contentId)
    {
        var userContent = _context.UserContents.Where(c => c.AccountId == accountId).Where(c => c.ContentId == contentId).FirstOrDefault();
        if (userContent == null) throw new KeyNotFoundException("UserContent not found");
        return userContent;
    }
}